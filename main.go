package main

import (
	"crypto/tls"
	"strings"
	"log"
	"net/http"
	"os"

	"gitlab.com/cty3000/superman-detector/supermandetector"
)

func getPort() string {
	p := os.Getenv("PORT")
	if p != "" {
		return p
	}

	return "80"
}

func getEndPoint() string {
	h := os.Getenv("HOST")
	if h != "" {
		return h + ":" + getPort()
	}

	return "0.0.0.0:" + getPort()
}

func getUrl() string {
	p := os.Getenv("PROTOCOL")
	if p != "" {
		return p + "://" + getEndPoint() + "/"
	}

	return "http://" + getEndPoint() + "/"
}

func main() {
	endpoint := getEndPoint()
	url := getUrl()

	impl, err := NewSupermanDetectorImpl(url)
	if err != nil {
		panic(err)
	}

	http.Handle("/", supermandetector.Init(impl, url, impl))

	if strings.HasPrefix(url, "https") {
		config, err := TLSConfiguration()
		if err != nil {
			log.Fatal("Cannot set up TLS: " + err.Error())
		}
		listener, err := tls.Listen("tcp", endpoint, config)
		if err != nil {
			panic(err)
		}
		log.Fatal(http.Serve(listener, nil))
	} else {
		log.Fatal(http.ListenAndServe(endpoint, nil))
	}
}
